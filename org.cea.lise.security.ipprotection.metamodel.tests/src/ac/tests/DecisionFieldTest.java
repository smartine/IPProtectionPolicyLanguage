/**
 */
package ac.tests;

import ac.DecisionField;

import junit.framework.TestCase;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Decision Field</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class DecisionFieldTest extends TestCase {

	/**
	 * The fixture for this Decision Field test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DecisionField fixture = null;

	/**
	 * Constructs a new Decision Field test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DecisionFieldTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Decision Field test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(DecisionField fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Decision Field test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DecisionField getFixture() {
		return fixture;
	}

} //DecisionFieldTest
