/**
 */
package ac.tests;

import ac.ConditionField;

import junit.framework.TestCase;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Condition Field</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class ConditionFieldTest extends TestCase {

	/**
	 * The fixture for this Condition Field test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConditionField fixture = null;

	/**
	 * Constructs a new Condition Field test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConditionFieldTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Condition Field test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(ConditionField fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Condition Field test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConditionField getFixture() {
		return fixture;
	}

} //ConditionFieldTest
