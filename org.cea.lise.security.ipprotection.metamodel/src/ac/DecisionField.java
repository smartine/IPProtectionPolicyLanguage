/**
 */
package ac;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Decision Field</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ac.AcPackage#getDecisionField()
 * @model abstract="true"
 * @generated
 */
public interface DecisionField extends EObject {
} // DecisionField
