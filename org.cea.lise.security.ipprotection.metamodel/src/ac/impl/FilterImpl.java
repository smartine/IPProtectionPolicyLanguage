/**
 */
package ac.impl;

import ac.AcPackage;
import ac.Filter;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Filter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ac.impl.FilterImpl#getFilterFuntion <em>Filter Funtion</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FilterImpl extends ObligationImpl implements Filter {
	/**
	 * The default value of the '{@link #getFilterFuntion() <em>Filter Funtion</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFilterFuntion()
	 * @generated
	 * @ordered
	 */
	protected static final String FILTER_FUNTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getFilterFuntion() <em>Filter Funtion</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFilterFuntion()
	 * @generated
	 * @ordered
	 */
	protected String filterFuntion = FILTER_FUNTION_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FilterImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AcPackage.Literals.FILTER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getFilterFuntion() {
		return filterFuntion;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFilterFuntion(String newFilterFuntion) {
		String oldFilterFuntion = filterFuntion;
		filterFuntion = newFilterFuntion;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AcPackage.FILTER__FILTER_FUNTION, oldFilterFuntion, filterFuntion));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AcPackage.FILTER__FILTER_FUNTION:
				return getFilterFuntion();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AcPackage.FILTER__FILTER_FUNTION:
				setFilterFuntion((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AcPackage.FILTER__FILTER_FUNTION:
				setFilterFuntion(FILTER_FUNTION_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AcPackage.FILTER__FILTER_FUNTION:
				return FILTER_FUNTION_EDEFAULT == null ? filterFuntion != null : !FILTER_FUNTION_EDEFAULT.equals(filterFuntion);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (filterFuntion: ");
		result.append(filterFuntion);
		result.append(')');
		return result.toString();
	}

} //FilterImpl
