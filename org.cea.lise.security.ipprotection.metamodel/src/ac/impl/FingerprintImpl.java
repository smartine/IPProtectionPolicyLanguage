/**
 */
package ac.impl;

import ac.AcPackage;
import ac.Fingerprint;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Fingerprint</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class FingerprintImpl extends ObligationImpl implements Fingerprint {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FingerprintImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AcPackage.Literals.FINGERPRINT;
	}

} //FingerprintImpl
