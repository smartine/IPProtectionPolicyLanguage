/**
 */
package ac.impl;

import ac.AcPackage;
import ac.Recommendation;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Recommendation</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class RecommendationImpl extends DecisionFieldImpl implements Recommendation {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RecommendationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AcPackage.Literals.RECOMMENDATION;
	}

} //RecommendationImpl
