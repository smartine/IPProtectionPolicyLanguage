/**
 */
package ac.impl;

import ac.AcPackage;
import ac.Encrypt;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Encrypt</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class EncryptImpl extends ObligationImpl implements Encrypt {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EncryptImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AcPackage.Literals.ENCRYPT;
	}

} //EncryptImpl
