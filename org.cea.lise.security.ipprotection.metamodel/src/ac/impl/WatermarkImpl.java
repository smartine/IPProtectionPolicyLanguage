/**
 */
package ac.impl;

import ac.AcPackage;
import ac.Watermark;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Watermark</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class WatermarkImpl extends ObligationImpl implements Watermark {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected WatermarkImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AcPackage.Literals.WATERMARK;
	}

} //WatermarkImpl
