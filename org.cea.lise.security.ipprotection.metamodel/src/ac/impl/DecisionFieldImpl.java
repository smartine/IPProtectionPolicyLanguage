/**
 */
package ac.impl;

import ac.AcPackage;
import ac.DecisionField;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Decision Field</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class DecisionFieldImpl extends MinimalEObjectImpl.Container implements DecisionField {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DecisionFieldImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AcPackage.Literals.DECISION_FIELD;
	}

} //DecisionFieldImpl
