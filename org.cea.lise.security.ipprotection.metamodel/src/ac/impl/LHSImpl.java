/**
 */
package ac.impl;

import ac.AcPackage;
import ac.ConditionField;
import ac.LHS;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>LHS</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ac.impl.LHSImpl#getConditionfields <em>Conditionfields</em>}</li>
 * </ul>
 *
 * @generated
 */
public class LHSImpl extends MinimalEObjectImpl.Container implements LHS {
	/**
	 * The cached value of the '{@link #getConditionfields() <em>Conditionfields</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConditionfields()
	 * @generated
	 * @ordered
	 */
	protected EList<ConditionField> conditionfields;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LHSImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AcPackage.Literals.LHS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ConditionField> getConditionfields() {
		if (conditionfields == null) {
			conditionfields = new EObjectContainmentEList<ConditionField>(ConditionField.class, this, AcPackage.LHS__CONDITIONFIELDS);
		}
		return conditionfields;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AcPackage.LHS__CONDITIONFIELDS:
				return ((InternalEList<?>)getConditionfields()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AcPackage.LHS__CONDITIONFIELDS:
				return getConditionfields();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AcPackage.LHS__CONDITIONFIELDS:
				getConditionfields().clear();
				getConditionfields().addAll((Collection<? extends ConditionField>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AcPackage.LHS__CONDITIONFIELDS:
				getConditionfields().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AcPackage.LHS__CONDITIONFIELDS:
				return conditionfields != null && !conditionfields.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //LHSImpl
