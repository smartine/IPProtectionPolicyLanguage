/**
 */
package ac.impl;

import ac.AcPackage;
import ac.Obligation;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Obligation</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ObligationImpl extends DecisionFieldImpl implements Obligation {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ObligationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AcPackage.Literals.OBLIGATION;
	}

} //ObligationImpl
