/**
 */
package ac;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Access Decision</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ac.AccessDecision#getDecision <em>Decision</em>}</li>
 * </ul>
 *
 * @see ac.AcPackage#getAccessDecision()
 * @model
 * @generated
 */
public interface AccessDecision extends DecisionField {
	/**
	 * Returns the value of the '<em><b>Decision</b></em>' attribute.
	 * The default value is <code>"Accept"</code>.
	 * The literals are from the enumeration {@link ac.DecisionKind}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Decision</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Decision</em>' attribute.
	 * @see ac.DecisionKind
	 * @see #setDecision(DecisionKind)
	 * @see ac.AcPackage#getAccessDecision_Decision()
	 * @model default="Accept" required="true"
	 * @generated
	 */
	DecisionKind getDecision();

	/**
	 * Sets the value of the '{@link ac.AccessDecision#getDecision <em>Decision</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Decision</em>' attribute.
	 * @see ac.DecisionKind
	 * @see #getDecision()
	 * @generated
	 */
	void setDecision(DecisionKind value);

} // AccessDecision
