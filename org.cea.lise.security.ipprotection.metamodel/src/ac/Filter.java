/**
 */
package ac;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Filter</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ac.Filter#getFilterFuntion <em>Filter Funtion</em>}</li>
 * </ul>
 *
 * @see ac.AcPackage#getFilter()
 * @model
 * @generated
 */
public interface Filter extends Obligation {
	/**
	 * Returns the value of the '<em><b>Filter Funtion</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Filter Funtion</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Filter Funtion</em>' attribute.
	 * @see #setFilterFuntion(String)
	 * @see ac.AcPackage#getFilter_FilterFuntion()
	 * @model
	 * @generated
	 */
	String getFilterFuntion();

	/**
	 * Sets the value of the '{@link ac.Filter#getFilterFuntion <em>Filter Funtion</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Filter Funtion</em>' attribute.
	 * @see #getFilterFuntion()
	 * @generated
	 */
	void setFilterFuntion(String value);

} // Filter
