/**
 */
package ac;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see ac.AcFactory
 * @model kind="package"
 * @generated
 */
public interface AcPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "ac";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "ac";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "ac";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	AcPackage eINSTANCE = ac.impl.AcPackageImpl.init();

	/**
	 * The meta object id for the '{@link ac.impl.PolicyImpl <em>Policy</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ac.impl.PolicyImpl
	 * @see ac.impl.AcPackageImpl#getPolicy()
	 * @generated
	 */
	int POLICY = 0;

	/**
	 * The feature id for the '<em><b>Rules</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POLICY__RULES = 0;

	/**
	 * The feature id for the '<em><b>Default Policy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POLICY__DEFAULT_POLICY = 1;

	/**
	 * The number of structural features of the '<em>Policy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POLICY_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Policy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POLICY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ac.impl.RuleImpl <em>Rule</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ac.impl.RuleImpl
	 * @see ac.impl.AcPackageImpl#getRule()
	 * @generated
	 */
	int RULE = 1;

	/**
	 * The feature id for the '<em><b>Lhs</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE__LHS = 0;

	/**
	 * The feature id for the '<em><b>Rhs</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE__RHS = 1;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE__ID = 2;

	/**
	 * The number of structural features of the '<em>Rule</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Rule</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ac.impl.LHSImpl <em>LHS</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ac.impl.LHSImpl
	 * @see ac.impl.AcPackageImpl#getLHS()
	 * @generated
	 */
	int LHS = 2;

	/**
	 * The feature id for the '<em><b>Conditionfields</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LHS__CONDITIONFIELDS = 0;

	/**
	 * The number of structural features of the '<em>LHS</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LHS_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>LHS</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LHS_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ac.impl.RHSImpl <em>RHS</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ac.impl.RHSImpl
	 * @see ac.impl.AcPackageImpl#getRHS()
	 * @generated
	 */
	int RHS = 3;

	/**
	 * The feature id for the '<em><b>Decisions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RHS__DECISIONS = 0;

	/**
	 * The number of structural features of the '<em>RHS</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RHS_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>RHS</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RHS_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ac.impl.ConditionFieldImpl <em>Condition Field</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ac.impl.ConditionFieldImpl
	 * @see ac.impl.AcPackageImpl#getConditionField()
	 * @generated
	 */
	int CONDITION_FIELD = 4;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_FIELD__ATTRIBUTES = 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_FIELD__ID = 1;

	/**
	 * The number of structural features of the '<em>Condition Field</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_FIELD_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Condition Field</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_FIELD_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ac.impl.AttributeImpl <em>Attribute</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ac.impl.AttributeImpl
	 * @see ac.impl.AcPackageImpl#getAttribute()
	 * @generated
	 */
	int ATTRIBUTE = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE__NAME = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ac.impl.SubjectImpl <em>Subject</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ac.impl.SubjectImpl
	 * @see ac.impl.AcPackageImpl#getSubject()
	 * @generated
	 */
	int SUBJECT = 6;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBJECT__ATTRIBUTES = CONDITION_FIELD__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBJECT__ID = CONDITION_FIELD__ID;

	/**
	 * The number of structural features of the '<em>Subject</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBJECT_FEATURE_COUNT = CONDITION_FIELD_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Subject</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBJECT_OPERATION_COUNT = CONDITION_FIELD_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ac.impl.ObjectImpl <em>Object</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ac.impl.ObjectImpl
	 * @see ac.impl.AcPackageImpl#getObject()
	 * @generated
	 */
	int OBJECT = 7;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT__ATTRIBUTES = CONDITION_FIELD__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT__ID = CONDITION_FIELD__ID;

	/**
	 * The number of structural features of the '<em>Object</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_FEATURE_COUNT = CONDITION_FIELD_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Object</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_OPERATION_COUNT = CONDITION_FIELD_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ac.impl.ActionImpl <em>Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ac.impl.ActionImpl
	 * @see ac.impl.AcPackageImpl#getAction()
	 * @generated
	 */
	int ACTION = 8;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__ATTRIBUTES = CONDITION_FIELD__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__ID = CONDITION_FIELD__ID;

	/**
	 * The number of structural features of the '<em>Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_FEATURE_COUNT = CONDITION_FIELD_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_OPERATION_COUNT = CONDITION_FIELD_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ac.impl.DecisionFieldImpl <em>Decision Field</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ac.impl.DecisionFieldImpl
	 * @see ac.impl.AcPackageImpl#getDecisionField()
	 * @generated
	 */
	int DECISION_FIELD = 9;

	/**
	 * The number of structural features of the '<em>Decision Field</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECISION_FIELD_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Decision Field</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECISION_FIELD_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ac.impl.AccessDecisionImpl <em>Access Decision</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ac.impl.AccessDecisionImpl
	 * @see ac.impl.AcPackageImpl#getAccessDecision()
	 * @generated
	 */
	int ACCESS_DECISION = 10;

	/**
	 * The feature id for the '<em><b>Decision</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_DECISION__DECISION = DECISION_FIELD_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Access Decision</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_DECISION_FEATURE_COUNT = DECISION_FIELD_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Access Decision</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACCESS_DECISION_OPERATION_COUNT = DECISION_FIELD_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ac.impl.ObligationImpl <em>Obligation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ac.impl.ObligationImpl
	 * @see ac.impl.AcPackageImpl#getObligation()
	 * @generated
	 */
	int OBLIGATION = 11;

	/**
	 * The number of structural features of the '<em>Obligation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBLIGATION_FEATURE_COUNT = DECISION_FIELD_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Obligation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBLIGATION_OPERATION_COUNT = DECISION_FIELD_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ac.impl.RecommendationImpl <em>Recommendation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ac.impl.RecommendationImpl
	 * @see ac.impl.AcPackageImpl#getRecommendation()
	 * @generated
	 */
	int RECOMMENDATION = 12;

	/**
	 * The number of structural features of the '<em>Recommendation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECOMMENDATION_FEATURE_COUNT = DECISION_FIELD_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Recommendation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECOMMENDATION_OPERATION_COUNT = DECISION_FIELD_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ac.impl.WatermarkImpl <em>Watermark</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ac.impl.WatermarkImpl
	 * @see ac.impl.AcPackageImpl#getWatermark()
	 * @generated
	 */
	int WATERMARK = 13;

	/**
	 * The number of structural features of the '<em>Watermark</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WATERMARK_FEATURE_COUNT = OBLIGATION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Watermark</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WATERMARK_OPERATION_COUNT = OBLIGATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ac.impl.FingerprintImpl <em>Fingerprint</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ac.impl.FingerprintImpl
	 * @see ac.impl.AcPackageImpl#getFingerprint()
	 * @generated
	 */
	int FINGERPRINT = 14;

	/**
	 * The number of structural features of the '<em>Fingerprint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINGERPRINT_FEATURE_COUNT = OBLIGATION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Fingerprint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FINGERPRINT_OPERATION_COUNT = OBLIGATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ac.impl.EncryptImpl <em>Encrypt</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ac.impl.EncryptImpl
	 * @see ac.impl.AcPackageImpl#getEncrypt()
	 * @generated
	 */
	int ENCRYPT = 15;

	/**
	 * The number of structural features of the '<em>Encrypt</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENCRYPT_FEATURE_COUNT = OBLIGATION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Encrypt</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENCRYPT_OPERATION_COUNT = OBLIGATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ac.impl.FilterImpl <em>Filter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ac.impl.FilterImpl
	 * @see ac.impl.AcPackageImpl#getFilter()
	 * @generated
	 */
	int FILTER = 16;

	/**
	 * The feature id for the '<em><b>Filter Funtion</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILTER__FILTER_FUNTION = OBLIGATION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILTER_FEATURE_COUNT = OBLIGATION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILTER_OPERATION_COUNT = OBLIGATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ac.DefaultPolicy <em>Default Policy</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ac.DefaultPolicy
	 * @see ac.impl.AcPackageImpl#getDefaultPolicy()
	 * @generated
	 */
	int DEFAULT_POLICY = 17;

	/**
	 * The meta object id for the '{@link ac.DecisionKind <em>Decision Kind</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ac.DecisionKind
	 * @see ac.impl.AcPackageImpl#getDecisionKind()
	 * @generated
	 */
	int DECISION_KIND = 18;


	/**
	 * Returns the meta object for class '{@link ac.Policy <em>Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Policy</em>'.
	 * @see ac.Policy
	 * @generated
	 */
	EClass getPolicy();

	/**
	 * Returns the meta object for the containment reference list '{@link ac.Policy#getRules <em>Rules</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Rules</em>'.
	 * @see ac.Policy#getRules()
	 * @see #getPolicy()
	 * @generated
	 */
	EReference getPolicy_Rules();

	/**
	 * Returns the meta object for the attribute '{@link ac.Policy#getDefaultPolicy <em>Default Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Default Policy</em>'.
	 * @see ac.Policy#getDefaultPolicy()
	 * @see #getPolicy()
	 * @generated
	 */
	EAttribute getPolicy_DefaultPolicy();

	/**
	 * Returns the meta object for class '{@link ac.Rule <em>Rule</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rule</em>'.
	 * @see ac.Rule
	 * @generated
	 */
	EClass getRule();

	/**
	 * Returns the meta object for the containment reference '{@link ac.Rule#getLhs <em>Lhs</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Lhs</em>'.
	 * @see ac.Rule#getLhs()
	 * @see #getRule()
	 * @generated
	 */
	EReference getRule_Lhs();

	/**
	 * Returns the meta object for the containment reference '{@link ac.Rule#getRhs <em>Rhs</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Rhs</em>'.
	 * @see ac.Rule#getRhs()
	 * @see #getRule()
	 * @generated
	 */
	EReference getRule_Rhs();

	/**
	 * Returns the meta object for the attribute '{@link ac.Rule#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see ac.Rule#getId()
	 * @see #getRule()
	 * @generated
	 */
	EAttribute getRule_Id();

	/**
	 * Returns the meta object for class '{@link ac.LHS <em>LHS</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>LHS</em>'.
	 * @see ac.LHS
	 * @generated
	 */
	EClass getLHS();

	/**
	 * Returns the meta object for the containment reference list '{@link ac.LHS#getConditionfields <em>Conditionfields</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Conditionfields</em>'.
	 * @see ac.LHS#getConditionfields()
	 * @see #getLHS()
	 * @generated
	 */
	EReference getLHS_Conditionfields();

	/**
	 * Returns the meta object for class '{@link ac.RHS <em>RHS</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>RHS</em>'.
	 * @see ac.RHS
	 * @generated
	 */
	EClass getRHS();

	/**
	 * Returns the meta object for the containment reference list '{@link ac.RHS#getDecisions <em>Decisions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Decisions</em>'.
	 * @see ac.RHS#getDecisions()
	 * @see #getRHS()
	 * @generated
	 */
	EReference getRHS_Decisions();

	/**
	 * Returns the meta object for class '{@link ac.ConditionField <em>Condition Field</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Condition Field</em>'.
	 * @see ac.ConditionField
	 * @generated
	 */
	EClass getConditionField();

	/**
	 * Returns the meta object for the containment reference list '{@link ac.ConditionField#getAttributes <em>Attributes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Attributes</em>'.
	 * @see ac.ConditionField#getAttributes()
	 * @see #getConditionField()
	 * @generated
	 */
	EReference getConditionField_Attributes();

	/**
	 * Returns the meta object for the attribute '{@link ac.ConditionField#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see ac.ConditionField#getId()
	 * @see #getConditionField()
	 * @generated
	 */
	EAttribute getConditionField_Id();

	/**
	 * Returns the meta object for class '{@link ac.Attribute <em>Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Attribute</em>'.
	 * @see ac.Attribute
	 * @generated
	 */
	EClass getAttribute();

	/**
	 * Returns the meta object for the attribute '{@link ac.Attribute#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see ac.Attribute#getName()
	 * @see #getAttribute()
	 * @generated
	 */
	EAttribute getAttribute_Name();

	/**
	 * Returns the meta object for the attribute '{@link ac.Attribute#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see ac.Attribute#getValue()
	 * @see #getAttribute()
	 * @generated
	 */
	EAttribute getAttribute_Value();

	/**
	 * Returns the meta object for class '{@link ac.Subject <em>Subject</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Subject</em>'.
	 * @see ac.Subject
	 * @generated
	 */
	EClass getSubject();

	/**
	 * Returns the meta object for class '{@link ac.Object <em>Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Object</em>'.
	 * @see ac.Object
	 * @generated
	 */
	EClass getObject();

	/**
	 * Returns the meta object for class '{@link ac.Action <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Action</em>'.
	 * @see ac.Action
	 * @generated
	 */
	EClass getAction();

	/**
	 * Returns the meta object for class '{@link ac.DecisionField <em>Decision Field</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Decision Field</em>'.
	 * @see ac.DecisionField
	 * @generated
	 */
	EClass getDecisionField();

	/**
	 * Returns the meta object for class '{@link ac.AccessDecision <em>Access Decision</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Access Decision</em>'.
	 * @see ac.AccessDecision
	 * @generated
	 */
	EClass getAccessDecision();

	/**
	 * Returns the meta object for the attribute '{@link ac.AccessDecision#getDecision <em>Decision</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Decision</em>'.
	 * @see ac.AccessDecision#getDecision()
	 * @see #getAccessDecision()
	 * @generated
	 */
	EAttribute getAccessDecision_Decision();

	/**
	 * Returns the meta object for class '{@link ac.Obligation <em>Obligation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Obligation</em>'.
	 * @see ac.Obligation
	 * @generated
	 */
	EClass getObligation();

	/**
	 * Returns the meta object for class '{@link ac.Recommendation <em>Recommendation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Recommendation</em>'.
	 * @see ac.Recommendation
	 * @generated
	 */
	EClass getRecommendation();

	/**
	 * Returns the meta object for class '{@link ac.Watermark <em>Watermark</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Watermark</em>'.
	 * @see ac.Watermark
	 * @generated
	 */
	EClass getWatermark();

	/**
	 * Returns the meta object for class '{@link ac.Fingerprint <em>Fingerprint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Fingerprint</em>'.
	 * @see ac.Fingerprint
	 * @generated
	 */
	EClass getFingerprint();

	/**
	 * Returns the meta object for class '{@link ac.Encrypt <em>Encrypt</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Encrypt</em>'.
	 * @see ac.Encrypt
	 * @generated
	 */
	EClass getEncrypt();

	/**
	 * Returns the meta object for class '{@link ac.Filter <em>Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Filter</em>'.
	 * @see ac.Filter
	 * @generated
	 */
	EClass getFilter();

	/**
	 * Returns the meta object for the attribute '{@link ac.Filter#getFilterFuntion <em>Filter Funtion</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Filter Funtion</em>'.
	 * @see ac.Filter#getFilterFuntion()
	 * @see #getFilter()
	 * @generated
	 */
	EAttribute getFilter_FilterFuntion();

	/**
	 * Returns the meta object for enum '{@link ac.DefaultPolicy <em>Default Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Default Policy</em>'.
	 * @see ac.DefaultPolicy
	 * @generated
	 */
	EEnum getDefaultPolicy();

	/**
	 * Returns the meta object for enum '{@link ac.DecisionKind <em>Decision Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Decision Kind</em>'.
	 * @see ac.DecisionKind
	 * @generated
	 */
	EEnum getDecisionKind();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	AcFactory getAcFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link ac.impl.PolicyImpl <em>Policy</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ac.impl.PolicyImpl
		 * @see ac.impl.AcPackageImpl#getPolicy()
		 * @generated
		 */
		EClass POLICY = eINSTANCE.getPolicy();

		/**
		 * The meta object literal for the '<em><b>Rules</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference POLICY__RULES = eINSTANCE.getPolicy_Rules();

		/**
		 * The meta object literal for the '<em><b>Default Policy</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute POLICY__DEFAULT_POLICY = eINSTANCE.getPolicy_DefaultPolicy();

		/**
		 * The meta object literal for the '{@link ac.impl.RuleImpl <em>Rule</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ac.impl.RuleImpl
		 * @see ac.impl.AcPackageImpl#getRule()
		 * @generated
		 */
		EClass RULE = eINSTANCE.getRule();

		/**
		 * The meta object literal for the '<em><b>Lhs</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RULE__LHS = eINSTANCE.getRule_Lhs();

		/**
		 * The meta object literal for the '<em><b>Rhs</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RULE__RHS = eINSTANCE.getRule_Rhs();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RULE__ID = eINSTANCE.getRule_Id();

		/**
		 * The meta object literal for the '{@link ac.impl.LHSImpl <em>LHS</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ac.impl.LHSImpl
		 * @see ac.impl.AcPackageImpl#getLHS()
		 * @generated
		 */
		EClass LHS = eINSTANCE.getLHS();

		/**
		 * The meta object literal for the '<em><b>Conditionfields</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LHS__CONDITIONFIELDS = eINSTANCE.getLHS_Conditionfields();

		/**
		 * The meta object literal for the '{@link ac.impl.RHSImpl <em>RHS</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ac.impl.RHSImpl
		 * @see ac.impl.AcPackageImpl#getRHS()
		 * @generated
		 */
		EClass RHS = eINSTANCE.getRHS();

		/**
		 * The meta object literal for the '<em><b>Decisions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RHS__DECISIONS = eINSTANCE.getRHS_Decisions();

		/**
		 * The meta object literal for the '{@link ac.impl.ConditionFieldImpl <em>Condition Field</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ac.impl.ConditionFieldImpl
		 * @see ac.impl.AcPackageImpl#getConditionField()
		 * @generated
		 */
		EClass CONDITION_FIELD = eINSTANCE.getConditionField();

		/**
		 * The meta object literal for the '<em><b>Attributes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONDITION_FIELD__ATTRIBUTES = eINSTANCE.getConditionField_Attributes();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONDITION_FIELD__ID = eINSTANCE.getConditionField_Id();

		/**
		 * The meta object literal for the '{@link ac.impl.AttributeImpl <em>Attribute</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ac.impl.AttributeImpl
		 * @see ac.impl.AcPackageImpl#getAttribute()
		 * @generated
		 */
		EClass ATTRIBUTE = eINSTANCE.getAttribute();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ATTRIBUTE__NAME = eINSTANCE.getAttribute_Name();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ATTRIBUTE__VALUE = eINSTANCE.getAttribute_Value();

		/**
		 * The meta object literal for the '{@link ac.impl.SubjectImpl <em>Subject</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ac.impl.SubjectImpl
		 * @see ac.impl.AcPackageImpl#getSubject()
		 * @generated
		 */
		EClass SUBJECT = eINSTANCE.getSubject();

		/**
		 * The meta object literal for the '{@link ac.impl.ObjectImpl <em>Object</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ac.impl.ObjectImpl
		 * @see ac.impl.AcPackageImpl#getObject()
		 * @generated
		 */
		EClass OBJECT = eINSTANCE.getObject();

		/**
		 * The meta object literal for the '{@link ac.impl.ActionImpl <em>Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ac.impl.ActionImpl
		 * @see ac.impl.AcPackageImpl#getAction()
		 * @generated
		 */
		EClass ACTION = eINSTANCE.getAction();

		/**
		 * The meta object literal for the '{@link ac.impl.DecisionFieldImpl <em>Decision Field</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ac.impl.DecisionFieldImpl
		 * @see ac.impl.AcPackageImpl#getDecisionField()
		 * @generated
		 */
		EClass DECISION_FIELD = eINSTANCE.getDecisionField();

		/**
		 * The meta object literal for the '{@link ac.impl.AccessDecisionImpl <em>Access Decision</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ac.impl.AccessDecisionImpl
		 * @see ac.impl.AcPackageImpl#getAccessDecision()
		 * @generated
		 */
		EClass ACCESS_DECISION = eINSTANCE.getAccessDecision();

		/**
		 * The meta object literal for the '<em><b>Decision</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACCESS_DECISION__DECISION = eINSTANCE.getAccessDecision_Decision();

		/**
		 * The meta object literal for the '{@link ac.impl.ObligationImpl <em>Obligation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ac.impl.ObligationImpl
		 * @see ac.impl.AcPackageImpl#getObligation()
		 * @generated
		 */
		EClass OBLIGATION = eINSTANCE.getObligation();

		/**
		 * The meta object literal for the '{@link ac.impl.RecommendationImpl <em>Recommendation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ac.impl.RecommendationImpl
		 * @see ac.impl.AcPackageImpl#getRecommendation()
		 * @generated
		 */
		EClass RECOMMENDATION = eINSTANCE.getRecommendation();

		/**
		 * The meta object literal for the '{@link ac.impl.WatermarkImpl <em>Watermark</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ac.impl.WatermarkImpl
		 * @see ac.impl.AcPackageImpl#getWatermark()
		 * @generated
		 */
		EClass WATERMARK = eINSTANCE.getWatermark();

		/**
		 * The meta object literal for the '{@link ac.impl.FingerprintImpl <em>Fingerprint</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ac.impl.FingerprintImpl
		 * @see ac.impl.AcPackageImpl#getFingerprint()
		 * @generated
		 */
		EClass FINGERPRINT = eINSTANCE.getFingerprint();

		/**
		 * The meta object literal for the '{@link ac.impl.EncryptImpl <em>Encrypt</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ac.impl.EncryptImpl
		 * @see ac.impl.AcPackageImpl#getEncrypt()
		 * @generated
		 */
		EClass ENCRYPT = eINSTANCE.getEncrypt();

		/**
		 * The meta object literal for the '{@link ac.impl.FilterImpl <em>Filter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ac.impl.FilterImpl
		 * @see ac.impl.AcPackageImpl#getFilter()
		 * @generated
		 */
		EClass FILTER = eINSTANCE.getFilter();

		/**
		 * The meta object literal for the '<em><b>Filter Funtion</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FILTER__FILTER_FUNTION = eINSTANCE.getFilter_FilterFuntion();

		/**
		 * The meta object literal for the '{@link ac.DefaultPolicy <em>Default Policy</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ac.DefaultPolicy
		 * @see ac.impl.AcPackageImpl#getDefaultPolicy()
		 * @generated
		 */
		EEnum DEFAULT_POLICY = eINSTANCE.getDefaultPolicy();

		/**
		 * The meta object literal for the '{@link ac.DecisionKind <em>Decision Kind</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ac.DecisionKind
		 * @see ac.impl.AcPackageImpl#getDecisionKind()
		 * @generated
		 */
		EEnum DECISION_KIND = eINSTANCE.getDecisionKind();

	}

} //AcPackage
