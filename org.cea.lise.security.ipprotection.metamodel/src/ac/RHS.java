/**
 */
package ac;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>RHS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ac.RHS#getDecisions <em>Decisions</em>}</li>
 * </ul>
 *
 * @see ac.AcPackage#getRHS()
 * @model
 * @generated
 */
public interface RHS extends EObject {
	/**
	 * Returns the value of the '<em><b>Decisions</b></em>' containment reference list.
	 * The list contents are of type {@link ac.DecisionField}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Decisions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Decisions</em>' containment reference list.
	 * @see ac.AcPackage#getRHS_Decisions()
	 * @model containment="true"
	 * @generated
	 */
	EList<DecisionField> getDecisions();

} // RHS
