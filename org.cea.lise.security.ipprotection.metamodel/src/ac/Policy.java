/**
 */
package ac;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Policy</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ac.Policy#getRules <em>Rules</em>}</li>
 *   <li>{@link ac.Policy#getDefaultPolicy <em>Default Policy</em>}</li>
 * </ul>
 *
 * @see ac.AcPackage#getPolicy()
 * @model
 * @generated
 */
public interface Policy extends EObject {
	/**
	 * Returns the value of the '<em><b>Rules</b></em>' containment reference list.
	 * The list contents are of type {@link ac.Rule}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rules</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rules</em>' containment reference list.
	 * @see ac.AcPackage#getPolicy_Rules()
	 * @model containment="true"
	 * @generated
	 */
	EList<Rule> getRules();

	/**
	 * Returns the value of the '<em><b>Default Policy</b></em>' attribute.
	 * The default value is <code>"Open"</code>.
	 * The literals are from the enumeration {@link ac.DefaultPolicy}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Default Policy</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Default Policy</em>' attribute.
	 * @see ac.DefaultPolicy
	 * @see #setDefaultPolicy(DefaultPolicy)
	 * @see ac.AcPackage#getPolicy_DefaultPolicy()
	 * @model default="Open"
	 * @generated
	 */
	DefaultPolicy getDefaultPolicy();

	/**
	 * Sets the value of the '{@link ac.Policy#getDefaultPolicy <em>Default Policy</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Default Policy</em>' attribute.
	 * @see ac.DefaultPolicy
	 * @see #getDefaultPolicy()
	 * @generated
	 */
	void setDefaultPolicy(DefaultPolicy value);

} // Policy
