/**
 */
package ac;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Object</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ac.AcPackage#getObject()
 * @model
 * @generated
 */
public interface Object extends ConditionField {
} // Object
