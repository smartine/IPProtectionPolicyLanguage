/**
 */
package ac;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Obligation</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ac.AcPackage#getObligation()
 * @model
 * @generated
 */
public interface Obligation extends DecisionField {
} // Obligation
