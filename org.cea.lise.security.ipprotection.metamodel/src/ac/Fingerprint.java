/**
 */
package ac;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Fingerprint</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ac.AcPackage#getFingerprint()
 * @model
 * @generated
 */
public interface Fingerprint extends Obligation {
} // Fingerprint
