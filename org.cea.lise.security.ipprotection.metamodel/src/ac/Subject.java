/**
 */
package ac;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Subject</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ac.AcPackage#getSubject()
 * @model
 * @generated
 */
public interface Subject extends ConditionField {
} // Subject
