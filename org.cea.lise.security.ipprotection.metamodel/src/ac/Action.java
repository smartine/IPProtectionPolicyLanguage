/**
 */
package ac;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Action</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ac.AcPackage#getAction()
 * @model
 * @generated
 */
public interface Action extends ConditionField {
} // Action
