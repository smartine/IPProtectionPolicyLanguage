/**
 */
package ac;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Encrypt</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ac.AcPackage#getEncrypt()
 * @model
 * @generated
 */
public interface Encrypt extends Obligation {
} // Encrypt
