/**
 */
package ac;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Recommendation</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ac.AcPackage#getRecommendation()
 * @model
 * @generated
 */
public interface Recommendation extends DecisionField {
} // Recommendation
