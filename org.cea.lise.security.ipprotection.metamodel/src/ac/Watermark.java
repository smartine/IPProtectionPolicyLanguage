/**
 */
package ac;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Watermark</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ac.AcPackage#getWatermark()
 * @model
 * @generated
 */
public interface Watermark extends Obligation {
} // Watermark
